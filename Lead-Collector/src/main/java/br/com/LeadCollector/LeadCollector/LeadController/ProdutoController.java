package br.com.LeadCollector.LeadCollector.LeadController;

import br.com.LeadCollector.LeadCollector.Models.LeadModel;
import br.com.LeadCollector.LeadCollector.Models.Produto;
import br.com.LeadCollector.LeadCollector.Service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/Produto")
public class ProdutoController {

@Autowired
   private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> BuscarLista()
    {
        return produtoService.BuscarListaProdutos();
    }

    @GetMapping("/{id}")
     public Produto BuscarProduto(@PathVariable  Integer id)
    {
        Optional<Produto> produtoOptional = produtoService.BuscarPorId(id);

        if(produtoOptional.isPresent())
        {
            return produtoOptional.get();

        }
        else
            {
                throw new ResponseStatusException(HttpStatus.NO_CONTENT);
            }
    }

    @PostMapping("/")
    public Produto inserirProduto(@RequestBody Produto produto)
    {
        return produtoService.SalvarProduto(produto);
    }

    @PutMapping("/{id}")
    public Produto ProdutoLead(@PathVariable Integer id, @RequestBody Produto produto)
    {
        produto.setId(id);
        Produto produtobjeto = produtoService.AtualizarProduto(produto);
        return produto;
    }

    @DeleteMapping("/{id}")
    public Optional<Produto>  DeletarLead(@PathVariable Integer id)
    {
        Optional<Produto> produtoOptional = produtoService.BuscarPorId(id);
        if(produtoOptional.isPresent())
        {
            produtoService.DeletarProduto(produtoOptional.get());
        }
        return produtoOptional;
    }
}
