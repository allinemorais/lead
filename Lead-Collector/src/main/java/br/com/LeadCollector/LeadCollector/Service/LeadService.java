package br.com.LeadCollector.LeadCollector.Service;

import br.com.LeadCollector.LeadCollector.Enum.TipodeLead;
import br.com.LeadCollector.LeadCollector.Models.LeadModel;
import br.com.LeadCollector.LeadCollector.repositories.LeadRepository;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public  Optional<LeadModel>  buscarPorId(Integer id){
       Optional<LeadModel> leadOptional = leadRepository.findById(id);
      return  leadOptional;

    }

    public LeadModel SalvarLead(LeadModel lead)
    {
        LeadModel leadobjeto = leadRepository.save(lead);
        return leadobjeto;
    }

    public  Iterable<LeadModel> BuscarLeads()
    {
        Iterable<LeadModel> lstLeads  =  leadRepository.findAll();
        return  lstLeads;
    }

    public LeadModel AtualizarLead(LeadModel lead)
    {
        Optional<LeadModel> leadOptional = buscarPorId(lead.getId());
        LeadModel leadData = leadOptional.get();

        if(leadOptional.isPresent())
        {
            if(lead.getNome() == null)
            {
                lead.setNome(leadData.getNome());
            }

            if(lead.getEmail() == null)
            {
                lead.setEmail(leadData.getEmail());
            }

            if(lead.getTipoLead() == null)
            {
                lead.setTipoLead(leadData.getTipoLead());
            }

        }

        LeadModel leadobj = leadRepository.save(lead);
        return lead;
    }

    public LeadModel DeletarLead(LeadModel lead)
    {
        leadRepository.delete(lead);
        return lead;
    }
}

