package br.com.LeadCollector.LeadCollector.Service;

import br.com.LeadCollector.LeadCollector.Models.LeadModel;
import br.com.LeadCollector.LeadCollector.Models.Produto;
import br.com.LeadCollector.LeadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
   private ProdutoRepository produtoRepository;

    public Optional<Produto> BuscarPorId(Integer id)
    {
        Optional<Produto> optProduto = produtoRepository.findById(id);
        return optProduto;
    }

    public Produto SalvarProduto(Produto produto)
    {
        Produto produtoobjeto =  produtoRepository.save(produto);
        return produtoobjeto;
    }

    public Iterable<Produto> BuscarListaProdutos()
    {
        Iterable<Produto> lstProdutos = produtoRepository.findAll();
        return lstProdutos;
    }

    public Produto AtualizarProduto(Produto produto)
    {
        Optional<Produto> produtoOpt = BuscarPorId(produto.getId());

        if(produtoOpt.isPresent()){
            Produto produtoDados = produtoOpt.get();
            if(produto.getNome() == "")
            {
                produto.setNome(produtoDados.getNome());
            }
            if(produto.getDescricao() == "")
            {
                produto.setDescricao(produtoDados.getDescricao());
            }
            if(produto.getPreco() == 0.0)
            {
                produto.setPreco(produtoDados.getPreco());
            }
        }

        Produto produtoObjt = produtoRepository.save(produto);
        return produto;

    }

    public Produto DeletarProduto(Produto produto)
    {
        produtoRepository.delete(produto);
        return produto;
    }
}
