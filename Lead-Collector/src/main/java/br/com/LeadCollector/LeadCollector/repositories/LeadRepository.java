package br.com.LeadCollector.LeadCollector.repositories;

import br.com.LeadCollector.LeadCollector.Models.LeadModel;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<LeadModel,Integer> {
}
