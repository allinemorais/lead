package br.com.LeadCollector.LeadCollector.Models;

import br.com.LeadCollector.LeadCollector.Enum.TipodeLead;

import javax.persistence.*;

//Marca que a classe será uma tabela
@Entity
public class LeadModel {
    //Marca que este campo será preenchido automaticamente e será o id da tabela
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //Cria com o nome da coluna
    @Column(name = "nome_completo")
    private String nome;
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private TipodeLead tipoLead;

    public LeadModel() {

    }

    public LeadModel(String nome, String email, TipodeLead tipoLead) {
        this.nome = nome;
        this.email = email;
        this.tipoLead = tipoLead;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipodeLead getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipodeLead tipoLead) {
        this.tipoLead = tipoLead;
    }


}
