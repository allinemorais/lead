package br.com.LeadCollector.LeadCollector.repositories;

import br.com.LeadCollector.LeadCollector.Models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
