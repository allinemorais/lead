package br.com.LeadCollector.LeadCollector.LeadController;

import br.com.LeadCollector.LeadCollector.Models.LeadModel;
import br.com.LeadCollector.LeadCollector.Service.LeadService;
import br.com.LeadCollector.LeadCollector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/Lead")
public class LeadController {

    public LeadController() {
    }

    @Autowired
    private LeadService leadService;

    @GetMapping
    public Iterable<LeadModel> buscarTodosLeads(){
        return leadService.BuscarLeads();
    }


    @GetMapping("/{id}")
    public LeadModel buscarLead(@PathVariable Integer id){
        Optional<LeadModel> leadOptional = leadService.buscarPorId(id);

        if(leadOptional.isPresent()){
            return leadOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping("/")
    public LeadModel inserirLead(@RequestBody LeadModel lead)
    {
        LeadModel leadobjeto = leadService.SalvarLead(lead);
        return lead;
    }

    @PutMapping("/{id}")
    public LeadModel AtualizarLead(@PathVariable Integer id, @RequestBody LeadModel lead)
    {
        lead.setId(id);
        LeadModel leadobjeto = leadService.AtualizarLead(lead);
        return lead;
    }

    @DeleteMapping("/{id}")
    public Optional<LeadModel>  DeletarLead(@PathVariable Integer id)
    {
        Optional<LeadModel> leadOPT = leadService.buscarPorId(id);
        if(leadOPT.isPresent())
        {
            leadService.DeletarLead(leadOPT.get());
         }
        return leadOPT;
    }
}
